const dataUrl = 'https://run.mocky.io/v3/c7b4ca83-4aa2-42c7-92c2-f9170b36cb36';
export const isObject = (arg: any) => typeof arg === "object" && arg !== null;

// We do not know for sure what is gonna be sent to us;
export const getFormJsonFromApi = async (): Promise<object> => {
    try {
        const response = await fetch(dataUrl);
        if (response.ok) {
            const data = response.json();
            if (isObject(data)) {
                return data as object;
            } else {
                return {};
            }
        } else {
            return {};
        }
    } catch (e) {
        console.error(e);
        return {};
    }
}
