import React from 'react';
import { IRecognozedField } from '../model/state';
import { Field } from './form-controls/Field';
import { Form } from 'react-bootstrap';

export interface IPreviewProps {
    recognized: IRecognozedField[];
}

export const FormPreview: React.FC<IPreviewProps> = ({recognized}) => (
    <Form>
        {recognized.map(({type, label, items}, index) => {
            const safeItems = items ? items : [];
            return (
                <Field 
                    fieldType={type}
                    label={label}
                    items={safeItems}
                    key={index}
                />
            ) 
        })}
    </Form>
)
