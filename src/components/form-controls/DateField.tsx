import React from 'react';
import { Form } from 'react-bootstrap';

export const DateField: React.FC = () => (
    <Form.Control type="date" placeholder="Enter Date" />
)
