import React from 'react';
import { Form } from 'react-bootstrap';

interface IBooleanFieldProps {
    label: string;
}

export const BooleanField: React.FC<IBooleanFieldProps> = ({label}) => (
    <Form.Check 
        type={'checkbox'}
        label={label}
    />
)
