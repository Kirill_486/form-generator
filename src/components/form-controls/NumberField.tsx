import React from 'react';
import { Form } from 'react-bootstrap';

export const NumberField: React.FC = () => (
    <Form.Control type="number" placeholder="Enter Number here" />
)
