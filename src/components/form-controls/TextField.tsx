import React from 'react';
import { Form } from 'react-bootstrap';

export const TextField: React.FC = () => (
    <Form.Control as="textarea" rows={5}/>
)
