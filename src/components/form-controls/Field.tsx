import React from 'react';
import { BooleanField } from './BooleanField';
import { DateField } from './DateField';
import { NumberField } from './NumberField';
import { RadioGroup } from './RadioGroup';
import { StringField } from './StringField';
import { TextField } from './TextField';
import { Form } from 'react-bootstrap';

export enum FieldType {
    BooleanField = 'BooleanField',
    DateField = 'DateField',
    NumberField = 'NumberField',
    RadioGroup = 'RadioGroup',
    StringField = 'StringField',
    TextField = 'TextField',
}

const getFieldByKey = (key: FieldType, items: string[], label: string) => {
    switch (key) {
        case FieldType.BooleanField: {
            return <BooleanField label={label}/>;
        }
        case FieldType.DateField: {
            return <DateField />;
        }
        case FieldType.NumberField: {
            return <NumberField />;
        }
        case FieldType.RadioGroup: {
            return <RadioGroup items={items}/>;
        }
        case FieldType.StringField: {
            return <StringField />;
        }
        case FieldType.TextField: {
            return <TextField />;
        }
        default: {
            return;
        }
    }
}

interface FieldProps {
    fieldType: FieldType;
    label: string;
    items: string[];
}

export const Field: React.FC<FieldProps> = ({label, fieldType, items}) => {
    return (
        <Form.Group controlId="field__container">
            <Form.Label>{label}</Form.Label>
            {getFieldByKey(fieldType, items, label)}
        </Form.Group>
    )
}
