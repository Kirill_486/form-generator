import React from 'react';
import { Form } from 'react-bootstrap';

export interface ITitleProps {
    label: string;
}

export const FormTitle: React.FC<ITitleProps> = ({label}) => (
    <Form>
        <Form.Label className="m-3 font-weight-bold">{label}</Form.Label>
    </Form>    
)
