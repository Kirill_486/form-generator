import React from 'react';
import { Form } from 'react-bootstrap';

export const StringField: React.FC = () => ( 
    <Form.Control type="text" placeholder="Input Text" />
)
