import React from 'react';
import { Row, Button } from 'react-bootstrap';

export interface IButtonGroupProps {
    controls: string[];
}

export const ButtonGroup: React.FC<IButtonGroupProps> = ({controls}) => (
    <>
        {(controls.length > 0) && (
            <Row>
                {controls.map((control) => <Button className="btn btn-lg m-3" key={control}>{control}</Button>)}
            </Row>
        )}
    </>
)
