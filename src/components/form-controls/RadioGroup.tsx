import React, { useState, Dispatch } from 'react';
import { Form } from 'react-bootstrap';

interface RadioGroupProps {
    items: string[];
}

const getRadioGroup = (items: string[], selected: string, setSelected: Dispatch<React.SetStateAction<string>>) => {
    return items.map((item) => {
        return (
            <Form.Check
                type={'radio'}
                id={`check-api-${item}`}
                label={item}
                key={item}
                onChange={setSelected.bind(null, item)}
                checked={item === selected}/>
        )
    } );
}

export const RadioGroup: React.FC<RadioGroupProps> = ({items}) => {
    const [selected, setSelected] = useState('');
    return (
        <Form.Group controlId="radio__container">
            {getRadioGroup(items, selected, setSelected)}
        </Form.Group>
    )
} 
