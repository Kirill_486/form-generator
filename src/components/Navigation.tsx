import React from 'react';
import { command } from '../use_cases/rootSaga';
import {Navbar, Nav} from 'react-bootstrap'

export interface INAvigatorProps {
    onConfigClick: command;
    onResultClick: command;
}

export const Navigation: React.FC<INAvigatorProps> = ({onConfigClick, onResultClick}) => (
    <Navbar expand="lg" variant="light" bg="light">
      <Navbar.Brand href="#">Form constructor</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <Nav.Link onClick={onConfigClick}>Configuration</Nav.Link>
                <Nav.Link onClick={onResultClick}>Result</Nav.Link>
            </Nav>
          </Navbar.Collapse>
    </Navbar>
)
