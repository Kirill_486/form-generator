import React from 'react';
import { Form } from "react-bootstrap";

export interface IErrorProps {
    message: string;
}

export const ErrorMessage: React.FC<IErrorProps> = ({message}) => (
    <Form.Text className="text-danger m-2">{message}</Form.Text>
)