import React from 'react';
import {command} from '../use_cases/rootSaga'
import { Form, Button } from 'react-bootstrap';

export interface JSONEditorStateProps {
    json: string;
}

export interface JSONEditorDispatchProps {
    apply: command;
    onChange: command;
    onReadAndApply: command;
}

export interface JSONEditorProps
extends JSONEditorStateProps,
JSONEditorDispatchProps
{};

export const JsonEditor: React.FC<JSONEditorProps> = ({apply, json, onChange, onReadAndApply}) => (
    <Form>
        <Form.Group controlId="exampleForm.ControlTextarea1">
            <Form.Row>
                <Button
                    className="json-editor__apply m-3"
                    onClick={apply}>
                    Apply Changes
                </Button>
                <Button
                    className="json-editor__apply m-3"
                    onClick={onReadAndApply}>
                    Read JSON From Server and then Apply
                </Button>
            </Form.Row>            
            <Form.Group>
                <Form.Control as="textarea" rows={30} onChange={onChange} value={json}/>
            </Form.Group>
        </Form.Group>
    </Form>
)
