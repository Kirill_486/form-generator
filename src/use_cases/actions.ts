import { IAction, UseCases } from "./rootSaga";
import { ActionCreator } from "redux";

export const toConfig: ActionCreator<IAction<undefined>> = () => ({
    type: UseCases.toConfig,
    payload: undefined,
})

export const toResults: ActionCreator<IAction<undefined>> = () => ({
    type: UseCases.toResults,
    payload: undefined,
})

export const readJSON: ActionCreator<IAction<undefined>> = () => ({
    type: UseCases.readJSON,
    payload: undefined,
})

export const modifyJSON: ActionCreator<IAction<string>> = (json: string) => ({
    type: UseCases.modifyJSON,
    payload: json,
})

export const readJSONFromServerAndApply: ActionCreator<IAction<undefined>> = () => ({
    type: UseCases.getJSONFromAPI,
    payload: undefined,
})
