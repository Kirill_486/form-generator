import { takeEvery, put, select, call } from '@redux-saga/core/effects';
import { Action } from 'redux';
import { setState, partialState } from '../model/actions';
import { AppState, IRecognozedField } from '../model/state';
import { AppPage } from '../model/reducer';
import { getFormJsonFromApi } from '../api/getFormJsonFromApi';

export type command = (...args: any) => void;
export type RawJSON = string;

export enum UseCases {
    toConfig = 'toConfig',
    toResults = 'toResults',
    
    readJSON = 'readJSON',
    modifyJSON = 'modifyJSON',

    getJSONFromAPI = 'getJSONFromAPI',
}

export interface IAction<P> extends Action {
    payload: P;
}

export function* rootSaga () {
    yield takeEvery(UseCases.toConfig, toConfigSaga);
    yield takeEvery(UseCases.toResults, toResultsSaga);

    yield takeEvery(UseCases.readJSON, readJSONSaga);
    yield takeEvery(UseCases.modifyJSON, modifyJSONSaga);

    yield takeEvery(UseCases.getJSONFromAPI, takeJSONFromApiAndReadSaga);
    
}

function* toConfigSaga (action: IAction<undefined>) {
    const newState: partialState = {
        page: AppPage.config
    }
    yield put(setState(newState));
}

function* toResultsSaga (action: IAction<undefined>) {
    const newState: partialState = {
        page: AppPage.result
    }
    yield put(setState(newState));
}

// Here we know just that it is an array
const parseItems = (items: any[]) => {
    const recognizedItemsOrUndefined = items.map((item: any): IRecognozedField | undefined => {
        const hasTypeAndLabel = item.type && item.label;
        const items = Array.isArray(item.items) ? item.items : undefined;
        if (hasTypeAndLabel) {
            const recognized = {
                type: item.type,
                label: item.label,
                items,
            }
            return recognized;
        } else {
            return undefined;
        }
    });
    const recognizedFields = recognizedItemsOrUndefined.filter((item: any) => !!item) as IRecognozedField[];
    return recognizedFields;
}

function* readJSONSaga (action?: IAction<string>) {
    const {json}: AppState = yield select();
   
    try {
        const diff: partialState = {};
        const parsed = JSON.parse(json);

        const parsedItems = parsed.items;

        if (parsedItems && Array.isArray(parsedItems)) {
            const recognized = parseItems(parsedItems);
            diff.recognized = recognized;
        } else {
            diff.recognized = [];
        }

        const parsedTitle = parsed.title;
        if (parsedTitle) {
            diff.title = parsedTitle
        } else {
            diff.title = "";
        }

        const parsedControls = parsed.controls;
        if (parsedControls && Array.isArray(parsedControls)) {
            diff.controls = parsedControls;
        } else {
            diff.controls = [];
        }

        yield put(setState(diff));
        yield successSaga();
    } catch(e) {
        yield errorSaga('Can not parse JSON');
    }
}

function* takeJSONFromApiSaga () {
    yield loadingSaga();
    const responseData: object = yield call(getFormJsonFromApi);
    const responseDataJSON: RawJSON = JSON.stringify(responseData, undefined, 2);
    
    if (responseDataJSON) {
        const diff: partialState = {
            json: responseDataJSON,
        };

        yield put(setState(diff));
    }

    yield loadingFinishedSaga();
}

function* takeJSONFromApiAndReadSaga(action: IAction<undefined>) {
    yield takeJSONFromApiSaga();
    yield readJSONSaga();
}

function* modifyJSONSaga (action: IAction<string>) {
    const newState: partialState = {
        json: action.payload,
    }
    yield put(setState(newState));
}

function* successSaga() {
    const newState: partialState = {
        errorMessage: "",
    }
    yield put(setState(newState));
}

function* errorSaga(errorMessage: string) {
    const newState: partialState = {
        errorMessage,
    }
    yield put(setState(newState));
}

function* loadingSaga() {
    const newState: partialState = {
        isLoading: true,
    }
    yield put(setState(newState));
}

function* loadingFinishedSaga() {
    const newState: partialState = {
        isLoading: false,
    }
    yield put(setState(newState));
}