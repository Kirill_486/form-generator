import { JSONEditorDispatchProps, JsonEditor, JSONEditorStateProps } from "../components/JsonEditor";
import { readJSON, modifyJSON, readJSONFromServerAndApply } from "../use_cases/actions";
import { connect } from "react-redux";
import { AppState } from "../model/state";
import { Dispatch, Action } from "redux";
import { SyntheticEvent } from "react";

const mapStateToProps = (state: AppState): JSONEditorStateProps => {
    return {
        json: state.json,
    }
}

const mapDispatchToProps = (dispatch: Dispatch<Action>): JSONEditorDispatchProps => {
    return {
        apply: () => {
            dispatch(readJSON())
        },
        onChange: (e: SyntheticEvent<HTMLTextAreaElement>) => {
            const json = getJsonOutOfChangedEvent(e);
            dispatch(modifyJSON(json));
        },
        onReadAndApply: () => dispatch(readJSONFromServerAndApply()),
    };
}

export const AppJsonEditor = connect(mapStateToProps, mapDispatchToProps)(JsonEditor)

const getJsonOutOfChangedEvent = (e: SyntheticEvent<HTMLTextAreaElement>) => {
    return e.currentTarget.value;
}