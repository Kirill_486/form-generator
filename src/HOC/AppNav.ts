import { INAvigatorProps, Navigation } from '../components/Navigation'
import { toConfig, toResults } from '../use_cases/actions';
import { connect } from 'react-redux'
import { Dispatch } from 'react';
import { Action } from 'redux';

const mapDispatchToProps = (dispatch: Dispatch<Action>): INAvigatorProps => {
    return {
        onConfigClick: () => dispatch(toConfig()),
        onResultClick: () => dispatch(toResults()),
    };
}

export const AppNav = connect(undefined, mapDispatchToProps)(Navigation)
