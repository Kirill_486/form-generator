import { connect } from 'react-redux'
import { FormPreview, IPreviewProps } from '../components/FormPreview';
import { AppState } from '../model/state';

const mapStateToProps = (state: AppState): IPreviewProps => {
    return {
        recognized: state.recognized,
    };
}

export const AppFormPreview = connect(mapStateToProps)(FormPreview);
