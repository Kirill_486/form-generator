import { connect } from 'react-redux'
import { AppState } from '../model/state';
import { ButtonGroup, IButtonGroupProps } from '../components/form-controls/ButtonGroup';

const mapStateToProps = (state: AppState): IButtonGroupProps => {
    return {
        controls: state.controls,
    };
}

export const AppControls = connect(mapStateToProps)(ButtonGroup);
