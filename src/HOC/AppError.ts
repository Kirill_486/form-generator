import { connect } from 'react-redux'
import { AppState } from '../model/state';
import { IErrorProps, ErrorMessage } from '../components/ErrorMessage';

const mapStateToProps = (state: AppState): IErrorProps => {
    return {
        message: state.errorMessage,
    };
}

export const AppFormError = connect(mapStateToProps)(ErrorMessage);
