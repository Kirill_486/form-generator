import { connect } from 'react-redux'
import { AppState } from '../model/state';
import { ITitleProps, FormTitle } from '../components/form-controls/FormTitle';

const mapStateToProps = (state: AppState): ITitleProps => {
    return {
        label: state.title,
    };
}

export const AppFormTitle = connect(mapStateToProps)(FormTitle);
