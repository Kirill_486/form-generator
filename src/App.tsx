import React, { useEffect, Dispatch } from 'react';
import './App.css';
import { connect } from 'react-redux';
import { AppState } from './model/state';
import { AppNav } from './HOC/AppNav';
import { AppJsonEditor } from './HOC/AppJsonEditor';
import { AppFormPreview } from './HOC/AppFormPreview';
import { AppPage } from './model/reducer';
import { command } from './use_cases/rootSaga';
import { readJSON } from './use_cases/actions';
import { Container } from 'react-bootstrap';
import { AppFormError } from './HOC/AppError';
import { AppFormTitle } from './HOC/AppTitle';
import { AppControls } from './HOC/AppControls';
import { Action } from 'redux';

interface AppPropsState {
  page: AppPage;
}

interface AppPropsDispatch {
  onAppWakeUp: command;
}

interface AppProps extends
AppPropsState,
AppPropsDispatch {}

export const App: React.FC<AppProps> = ({page, onAppWakeUp}) => {
  useEffect(() => {
    onAppWakeUp();
    return undefined; // cleanup function;
  }, [onAppWakeUp]);
  return (
    <Container>
        <AppNav />
        <AppFormError />
        {page === AppPage.config ? 
          <AppJsonEditor /> : 
          (
            <>
              <AppFormTitle />
              <AppFormPreview />
              <AppControls />
            </>
          
          )}
    </Container>
  )
}

const mapStateToProps = (state: AppState): AppPropsState => ({
  page: state.page,
});

const mapDispatchToProps = (dispatch: Dispatch<Action>): AppPropsDispatch => ({
  onAppWakeUp: () => dispatch(readJSON())
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
