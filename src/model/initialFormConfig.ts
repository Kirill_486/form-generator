import { IRecognozedField } from "./state";
import { FieldType } from "../components/form-controls/Field";

const initialFields: IRecognozedField[] = [
    {
        type: FieldType.BooleanField,
        label: 'BooleanField'
    },
    {
        type: FieldType.DateField,
        label: 'DateField'
    },
    {
        type: FieldType.NumberField,
        label: 'NumberField'
    },
    {
        type: FieldType.RadioGroup,
        label: 'RadioGroup',
        items: ['One', 'Two', 'Three', 'Four'],
    },
    {
        type: FieldType.StringField,
        label: 'StringField'
    },
    {
        type: FieldType.TextField,
        label: 'TextField'
    },
];

const initialTitle = 'Form Title Here';
const initialControls = ['Ok', 'Cancel'];



const initialConfig = {
    items: initialFields,
    title: initialTitle,
    controls: initialControls,
}

export const initialFormConfig = JSON.stringify(initialConfig, undefined, 2);