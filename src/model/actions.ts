import { ActionCreator } from "redux";
import { IAction } from "../use_cases/rootSaga";
import { AppState } from "./state";

export enum ActionTypes {
    SET = 'SET',
    RESET = 'RESET',
};

export type partialState = Partial<AppState>

export const setState: ActionCreator<IAction<partialState>> = (state: partialState) => ({
    type: ActionTypes.SET,
    payload: state,
});

export const resetState: ActionCreator<IAction<undefined>> = () => ({
    type: ActionTypes.SET,
    payload: undefined,
});
