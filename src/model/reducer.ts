import { AppState } from "./state";
import { IAction } from "../use_cases/rootSaga";
import { ActionTypes, partialState } from "./actions";
import { initialFormConfig } from "./initialFormConfig";

export enum AppPage {
    config = 'config',
    result = 'result',
}

const initialState: AppState = {
    json: initialFormConfig,
    page: AppPage.config,
    recognized: [],
    errorMessage: '',
    controls: [],
    title: '',
    isLoading: false,
};

export const reducer = (state: AppState = initialState, action: IAction<partialState>) => {
    switch (action.type) {
        case ActionTypes.SET: {
            return {
                ...state,
                ...action.payload,
            }
        }
        case ActionTypes.RESET: {
            return {
                ...initialState,
            }
        }
        default : {
            return state;
        }
    }
}