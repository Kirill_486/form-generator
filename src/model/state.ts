import { createStore, applyMiddleware, compose } from "redux";
import { reducer, AppPage } from "./reducer";
import { FieldType } from "../components/form-controls/Field";
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from "../use_cases/rootSaga";

export interface IRecognozedField {
    type: FieldType;
    items?: string[];
    label: string;
}

export interface AppState {
    json: string;
    page: AppPage;
    recognized: IRecognozedField[];
    title: string;
    controls: string[];
    errorMessage: string;
    isLoading: boolean;
}

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducer, composeEnhancers(applyMiddleware(sagaMiddleware)));
sagaMiddleware.run(rootSaga);
